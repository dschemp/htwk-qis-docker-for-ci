FROM python:3.8-slim
WORKDIR /app
COPY . .
RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt install -y python3-lxml
RUN pip install -r requirements.txt
