import sys, os
import pyQIS

htwk = pyQIS.HTWKLeipzigQIS()


def write_results(filepath, content):
    with open(filepath, "w", encoding="utf8") as f:
        f.write(content)


def run(username, password, path):
    print("Logging in...")
    logged_in = htwk.login(username, password)
    if not logged_in:
        print("Could not login!")
        sys.exit(3)
    else:
        print("Logged in!")
        results = htwk.fetch_results()
        result = ""
        print("Fetching results...")
        for semester in results[:2]:
            print(">", semester)
            result += "***{}***%0A%0A".format(semester)
            for grade in semester.grades:
                if semester.grade_average_exam_identifier in grade.exam_id: # Skip invalid 'Modulprüfungen'
                    continue
                result += "*{}*%0A".format(grade.description)
                result += "{}: `{}`%0A%0A".format(grade.exam_id, grade.grade)
            (_, grade) = semester.grade_average()
            result += "_Grade average:_ `{}`".format(grade)
            result += "%0A%0A%0A"
        htwk.logout()
        print("Logged out!")

        write_results(path, result)


def main(argv):
    if len(argv) != 1:
        print("Usage: run.py <path>")
        sys.exit(2)
    username = os.environ.get('USERNAME')
    password = os.environ.get('PASSWORD')
    path = argv[0]
    if username == None or password == None:
        print("Username or password not set!")
        sys.exit(3)
    run(username, password, path)


if __name__ == "__main__":
    main(sys.argv[1:])
